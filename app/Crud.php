<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crud extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'first_name', 'last_name', 'image'
    ];
}
